﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	[SerializeField] private AudioClip[] levelMusicChangeArray;

	private AudioClip currentClip;

	private AudioSource audioSource;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		audioSource = GetComponent<AudioSource>();

	}
	
	void OnLevelWasLoaded(int level)
	{
		if(levelMusicChangeArray[level] == null)
			return;

		AudioClip thisLevelMusic = levelMusicChangeArray[level];

		if(thisLevelMusic == currentClip)
			return;

		currentClip = thisLevelMusic;
		ChangeVolume(PlayerPrefsManager.GetMasterVolume());
		Debug.Log("Playing clip: " + thisLevelMusic);
		audioSource.clip = thisLevelMusic;
		audioSource.loop = true;
		audioSource.Play();

	}

	public void ChangeVolume(float value)
	{
		audioSource.volume = value;
	}

}
