﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeIn : MonoBehaviour {

	[SerializeField] private float fadeInTime = 1f;

	private Image fadePanel;
	private Color currentColor = Color.black;
	private static bool used = false;
	
	void Start ()
	{
		fadePanel = GetComponent<Image>();
		if(used)
		{
			fadePanel.enabled = false;
		}
		used = true;
	}

	void Update ()
	{
		if(Time.timeSinceLevelLoad < fadeInTime)
		{
			//Fade in
			currentColor.a -= (Time.deltaTime / fadeInTime);
			fadePanel.color = currentColor;
		}
		else {
			fadePanel.enabled = false;
		}
	}
}
