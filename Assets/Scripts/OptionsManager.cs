﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsManager : MonoBehaviour {

	[SerializeField] private Slider volumeSlider;
	[SerializeField] private LevelManager levelManager;
	[SerializeField] private Slider difficultySlider;

	private MusicManager musicManager;
	
	void Start ()
	{
		musicManager = GameObject.FindObjectOfType<MusicManager>();
		volumeSlider.value = PlayerPrefsManager.GetMasterVolume();
		difficultySlider.value = PlayerPrefsManager.GetDifficulty();
	}

	public void UpdateVolume()
	{
		if(musicManager == null)
		{
			Debug.LogWarning("Music Manager is not in scene!");
			return;
		}
		musicManager.ChangeVolume(volumeSlider.value);
	}

	public void SaveAndExit()
	{
		PlayerPrefsManager.SetMasterVolume(volumeSlider.value);
		PlayerPrefsManager.SetDifficulty(difficultySlider.value);
		levelManager.LoadLevel("01a Start");
	}

	public void SetDefaults()
	{
		volumeSlider.value = 1f;
		difficultySlider.value = 0f;
	}


}
