﻿using UnityEngine;
using System.Collections;

public class Splash : MonoBehaviour {

	[SerializeField] private AudioClip splashMusic;

	[Range(1, 5)]
	[SerializeField] private float splashScreenLength = 5f;


	void Start ()
	{
		AudioSource.PlayClipAtPoint(splashMusic, Vector3.zero);
		Invoke("LoadNextLevel", splashScreenLength);
	}



	void LoadNextLevel()
	{
		Application.LoadLevel(Application.loadedLevel + 1);
	}

}
